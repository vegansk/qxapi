package qxapi

import (
    "fmt"
)

////////////////////////////////////////////////////////////////////////////////////////////////////
// Module interface

// Qooxdoo API reader
type ApiReader interface {
    // Read API description
    Read() (*Api, error)
}

// Create API reader, based on API description files.
// Path may be the application path or the path to API's *.json files
func NewJsonReader(paths ...string) (ApiReader, error) {
    return nil, errNotImplemented
}

////////////////////////////////////////////////////////////////////////////////////////////////////
// API info interface

// API description
type Api struct {
}

// Package description
type Package struct {
}

// Class description
type Class struct {
}

// Interface description
type Interface struct {
}

// Mixin description
type Mixin struct {
}

// Get root package
func (a *Api) RootPackage() *Package {
    return nil
}

////////////////////////////////////////////////////////////////////////////////////////////////////
// Misc

// Errors
var (
    errNotImplemented = fmt.Errorf("Not implemented!")
)
